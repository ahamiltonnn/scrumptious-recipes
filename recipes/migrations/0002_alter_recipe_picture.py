# Generated by Django 5.0.1 on 2024-01-22 23:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('recipes', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='recipe',
            name='picture',
            field=models.URLField(max_length=500),
        ),
    ]
